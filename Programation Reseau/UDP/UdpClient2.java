import java.io.*;
import java.net.*;

public class UdpClient2{
    public static void main(String[] args) throws SocketException, IOException, UnknownHostException{
        String s=new String(args[2]);
        byte[] b;
        b = s.getBytes();
        InetAddress adr=InetAddress.getByName(args[0]);
        DatagramSocket DsE=new DatagramSocket();
        DatagramPacket DpE=new DatagramPacket(b,b.length,adr,Integer.parseInt(args[1]));
        DsE.send(DpE);
    }
}