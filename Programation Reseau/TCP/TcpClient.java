import java.net.*;
import java.io.*;
import java.util.*;

public class TcpClient{
    public static void main(String[] args) throws SocketException, IOException, UnknownHostException, InterruptedException{
        try{
            Scanner sc=new Scanner(System.in);
            Socket socket=new Socket(args[0],Integer.parseInt(args[1]));
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));  
            PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
            while(true){
                String s=br.readLine();
                if (s.equals("q"))break;
                System.out.println("Server : " + s); 
                System.out.print("Client : ");
                s=sc.nextLine();
                pw.println(s);
            }
            socket.close();
        }
        catch(NullPointerException e){
            System.exit(0);
        }
    }
}