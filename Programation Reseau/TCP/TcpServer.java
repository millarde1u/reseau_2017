import java.net.*;
import java.io.*;
import java.util.*;

public class TcpServer{
    public static void main(String[] args) throws SocketException, IOException, UnknownHostException, InterruptedException{
        try{
            Scanner sc=new Scanner(System.in);
            ServerSocket srv=new ServerSocket(Integer.parseInt(args[1]));
            Socket socket=srv.accept();
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));  
            PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
            System.out.print("Server : ");
            String s = sc.nextLine();
            while(true){
                pw.println(s);
                s=br.readLine();
                if(s.equals("q"))break;
                System.out.println("Client : "+s);
                System.out.print("Server : ");
                s=sc.nextLine();
            
            }
            br.close();
            pw.close();
            socket.close();
        }
        catch(NullPointerException e){
            System.exit(0);
        }
    }
}